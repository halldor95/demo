CC = g++
CFLAGS = -g -Wall
SRCS = kmeans.cpp
PROG = $(basename $(SRCS))

OPENCV = `pkg-config opencv --cflags --libs`
LIBS = $(OPENCV)

$(PROG):$(SRCS)
	$(CC) $(CFLAGS) -o $(PROG) $(SRCS) $(LIBS)

exec:
	./$(PROG)