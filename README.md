# About

An example that creates points and then do a K-means algorithm on top of them.  

The code is automatically compiled using `adnrv/opencv` docker image, and, on success, it runs the program `kmeans` to get the results.  It publishes the resultant image, `clusters.png` as an artifact on this repository.

